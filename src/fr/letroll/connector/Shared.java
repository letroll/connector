package fr.letroll.connector;

public class Shared {
	public enum messages_udp {
		DEBUG(0), RECEPTION(1), SENDING(2), ERROR(3);
		public final int code;
		messages_udp(int _code) {
			code = _code;
		}
	}
}
