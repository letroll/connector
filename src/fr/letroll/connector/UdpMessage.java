package fr.letroll.connector;

import java.net.InetAddress;
import java.util.Date;

public class UdpMessage
{
	  private Date mHour;
	  private InetAddress mIp;
	  private int mPort;
	  private String mText;

	  public UdpMessage()
	  {
	    this.mText = "";
	    this.mPort = 0;
	    this.mHour = new Date();
	  }

	  public UdpMessage(String paramString)
	  {
	    this.mText = paramString;
	    this.mPort = 0;
	    this.mHour = new Date();
	  }

	  public UdpMessage(String paramString, InetAddress paramInetAddress, int paramInt)
	  {
	    this.mText = paramString;
	    this.mIp = paramInetAddress;
	    this.mPort = paramInt;
	    this.mHour = new Date();
	  }

	  public Date getHour()
	  {
	    return this.mHour;
	  }

	  public InetAddress getIp()
	  {
	    return this.mIp;
	  }
	  
	  public String getIpString()
	  {
	    return (this.mIp!=null)?this.mIp.toString().substring(1):"";
	  }

	  public int getPort()
	  {
	    return this.mPort;
	  }

	  public String getTexto()
	  {
	    return this.mText;
	  }

	  public void setHour(Date paramDate)
	  {
	    this.mHour = paramDate;
	  }

	  public void setIp(InetAddress paramInetAddress)
	  {
	    this.mIp = paramInetAddress;
	  }

	  public void setPort(int paramInt)
	  {
	    this.mPort = paramInt;
	  }

	  public void setText(String paramString)
	  {
	    this.mText = paramString;
	  }
	}