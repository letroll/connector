package fr.letroll.connector;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.MulticastLock;
import android.util.Log;

import com.magic.debug.Logger;
import com.magic.system.Information;

public class UDPBroadcast {

	static abstract interface DiscoveryReceiver {
		public abstract void addAnnouncedServers(InetAddress[] paramArrayOfInetAddress, int[] paramArrayOfInt);
	}

	public UDPBroadcast(WifiManager wifimanager, int i) throws IOException {
		mWifi = wifimanager;
		mPort = i;
		ml = mWifi.createMulticastLock("multicastchat");
		ml.acquire();
		if (!obteningPort())
			Logger.loge(this, "Unable to reserve port");
	}

	private boolean obteningPort() {
		boolean flag = true;
		try {
			mSocket = new DatagramSocket(mPort);
			mSocket.setBroadcast(true);
			mSocket.setSoTimeout(0);
		} catch (SocketException socketexception) {
			Logger.loge(this, "I can not open socket");
			socketexception.printStackTrace();
			flag = false;
		}
		return flag;
	}

	public void send(UdpMessage paramMensaje) throws IOException {
		InetAddress localInetAddress = Information.getBroadcastAddress(mWifi);
		Log.d("UDPBroadcast", "Sending data: " + paramMensaje.getTexto());
		DatagramPacket localDatagramPacket = new DatagramPacket(paramMensaje.getTexto().getBytes("UTF-8"), paramMensaje.getTexto().getBytes("UTF-8").length, localInetAddress, this.mPort);
		try {
			this.mSocket.send(localDatagramPacket);
		} catch (SocketException localSocketException) {
			Logger.loge(this, "send failed. are you connected to wifi?");
			localSocketException.printStackTrace();
		}
	}

	public void die() {
		if (mSocket != null)
			if (!mSocket.isClosed())
				mSocket.close();
		try {
			if (ml.isHeld())
				ml.release();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public UdpMessage receive() {
		byte abyte0[] = new byte[512];
		DatagramPacket datagrampacket = new DatagramPacket(abyte0, abyte0.length);
		if (!mSocket.isClosed()) {
			try {
				mSocket.receive(datagrampacket);
			} catch (IOException ioexception) {
				// Log.d("UDPBroadcast", "in reception", ioexception);
			}
			String s1;
			try {
				s1 = new String(datagrampacket.getData(), 0, datagrampacket.getLength(), "UTF-8");
				return new UdpMessage(s1, datagrampacket.getAddress(), datagrampacket.getPort());
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private int mPort;
	private DatagramSocket mSocket;
	private WifiManager mWifi;
	private MulticastLock ml;

	public boolean isClose() {
		if (mSocket != null)
			return mSocket.isClosed();
		else
			return true;
	}
}
