package fr.letroll.connector;

import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

public class SendQueue extends Thread {
	private LinkedBlockingQueue<UdpMessage> sendQueue = new LinkedBlockingQueue<UdpMessage>();
	private boolean continu;
	private UDPBroadcast mSocket;

	public SendQueue(UDPBroadcast paramUDPBroadcast) {
		this.mSocket = paramUDPBroadcast;
		this.continu = true;
	}

	public boolean send(UdpMessage paramMensaje) {
		return this.sendQueue.offer(paramMensaje);
	}

	public void die() {
		this.continu = false;
	}

	public void run() {
		while (true) {
			if (!continu)
				return;
			try {
				this.mSocket.send((UdpMessage) this.sendQueue.take());
			} catch (IOException localIOException) {
				localIOException.printStackTrace();
			} catch (InterruptedException localInterruptedException) {
				localInterruptedException.printStackTrace();
			}
		}
	}
}