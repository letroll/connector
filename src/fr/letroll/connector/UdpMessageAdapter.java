package fr.letroll.connector;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class UdpMessageAdapter extends ArrayAdapter<UdpMessage>
{
  public UdpMessageAdapter(Context paramContext, int paramInt, List<UdpMessage> paramList)
  {
    super(paramContext, paramInt, paramList);
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    return super.getView(paramInt, paramView, paramViewGroup);
  }
}