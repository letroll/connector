package fr.letroll.connector;

import android.os.Handler;

public class ReceptionQueue extends Thread {
	private boolean continu;
	private Handler mHandler;
	private UDPBroadcast mSocket;

	public ReceptionQueue(UDPBroadcast paramUDPBroadcast, Handler paramHandler) {
		this.mSocket = paramUDPBroadcast;
		this.mHandler = paramHandler;
		this.continu = true;
	}

	public void die() {
		this.continu = false;
	}

	public void run() {
		while (true) {
			if (!this.continu)
				return;
			try {
				if (mSocket != null)
					if (!mSocket.isClose())
						mHandler.obtainMessage(Shared.messages_udp.RECEPTION.ordinal(), mSocket.receive()).sendToTarget();
			} catch (Exception e) {
				e.printStackTrace();
				die();
			}
		}
	}

	@Override
	public void interrupt() {
		super.interrupt();
		die();
	}

}