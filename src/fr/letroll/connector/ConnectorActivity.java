package fr.letroll.connector;

import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import com.magic.broadcastreceiver.NetworkStateReceiver;
import com.magic.debug.Logger;
import com.magic.debug.loggers.LogcatLogger;
import com.magic.dialogsample.Dialogs;
import com.magic.listener.ConnexionListener;
import com.magic.system.Information;

public class ConnectorActivity extends Activity implements ConnexionListener {

	private LogcatLogger log;
	private IntentFilter networkIntentFilter;
	private NetworkStateReceiver networkStateReceiver;
	private static SendQueue sendQueue;
	private static ReceptionQueue receptionQueue;
	private static UDPBroadcast mUDPBroadcastSocket;
	private static boolean server = true;
	private boolean refresh = false;
	private boolean registered = false;
	private String connexionType;
	private int port = 12543;
	private static Context mContext;
	private static Connector cor = null;
	private static ConnectorServer corServ = null;

	private static int doublePingFix = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mContext = this;
		log = new LogcatLogger();
		Logger.addLogger(log);

		networkIntentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
		networkStateReceiver = new NetworkStateReceiver(this);

		refreshTitle();
	}

	@Override
	protected void onResume() {
		super.onResume();
		Logger.loge(this, "onResume");
		if (!registered) {
			registerReceiver(networkStateReceiver, networkIntentFilter);
			registered = true;
		}

		try {
			mUDPBroadcastSocket = new UDPBroadcast((WifiManager) this.getSystemService(Context.WIFI_SERVICE), port);
		} catch (IOException ioexception) {
			ioexception.printStackTrace();
		}
		sendQueue = new SendQueue(mUDPBroadcastSocket);
		sendQueue.start();
		receptionQueue = new ReceptionQueue(mUDPBroadcastSocket, mHandler);
		receptionQueue.start();
	}

	public void connect(View v) {
		Logger.loge(this, "connect");
		UdpMessage udpMessage = new UdpMessage("hack");
		sendQueue.send(udpMessage);
		setServer(false);
	}

	public void setPort(int _port) {
		port = _port;
	}

	public void setConnector(Connector _cor) {
		cor = _cor;
	}

	public void setConnectorServ(ConnectorServer _corServ) {
		corServ = _corServ;
	}

	public static void connect() {
		Logger.loge(mContext, "connect");
		UdpMessage udpMessage = new UdpMessage("hack");
		sendQueue.send(udpMessage);
		setServer(false);
	}

	private void refreshTitle() {
		if (refresh) {
			Logger.loge(this, "refreshTitle " + Information.getLocalIp(this));
			setTitle(connexionType + ":" + Information.getLocalIp(this));
		}
	}

	private static Handler mHandler = new Handler() {
		public void handleMessage(Message paramAnonymousMessage) {
			super.handleMessage(paramAnonymousMessage);
			UdpMessage mess = ((UdpMessage) paramAnonymousMessage.obj);
			if (mess != null)
				if (mess.getTexto().equals("hack")) {
					if (corServ != null) {
						String ip = mess.getIpString();
						Logger.loge(this, "ip:" + ip);
						if(isServer())
						corServ.onServerPing(ip);
					} else {
						Logger.loge(this, "corServ:null");
					}
					UdpMessage udpMessage = new UdpMessage(mess.getIpString());
					sendQueue.send(udpMessage);
				} else {
					if (!isServer())
						if (cor != null) {
							doublePingFix++;
							if (doublePingFix == 2) {
								cor.onServerFind(mess.getIpString());
								doublePingFix = 0;
							}
						}
				}
		}
	};

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Logger.loge(this, "onDestroy");
		mUDPBroadcastSocket.die();
		if (registered) {
			unregisterReceiver(networkStateReceiver);
			registered = false;
		}
	}

	@Override
	public void onBackPressed() {
		Logger.loge(this, "onBackPressed");
		mUDPBroadcastSocket.die();
		if (registered) {
			unregisterReceiver(networkStateReceiver);
			registered = false;
		}
		super.onBackPressed();
	}

	@Override
	protected void onPause() {
		super.onPause();
		Logger.loge(this, "onPause");
		if (registered) {
			unregisterReceiver(networkStateReceiver);
			registered = false;
		}
	}

	public void onConnexionChange(String type) {
		Logger.loge(this, "onConnexionChange:" + type);
		connexionType = type;
		refreshTitle();
		Dialogs.dismissConnectionDialog();
	}

	public void onConnexionShutdown() {
		Logger.loge(this, "onConnexionShutdown");
		connexionType = "no connexion";
		refreshTitle();
		Dialogs.showNoConnectionDialog(this);
	}

	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}

	public static boolean isServer() {
	    return server;
    }

	public static void setServer(boolean server) {
	    ConnectorActivity.server = server;
    }

}
